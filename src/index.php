<?php
/*
* Copyright 2016 (C) Diglias AB
*
* @author jonas
*/

    require './inc/header.php';
?>
    <h1>Diglias GO PHP sample application</h1>
    <p> Click the link to authenticate:
        <a href="/authenticate">Authenticate</a>
    </p>
<?php require './inc/footer.php';
?>